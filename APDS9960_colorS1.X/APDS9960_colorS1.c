/*! \file  APDS9960_colorS1.c
 *
 *  \brief This file contains the mainline for APDS9960_colorS1
 *
 *
 *  \author jjmcd
 *  \date November 29, 2018, 9:23 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdio.h>
#include <string.h>
#include "APDS9960_colorS1.h"
#include "../include/graphicsHostLibrary.h"
#include "../include/colors.h"
#include "../include/directions.h"

#define LINE_DELAY 8000
#define SLOW

double RGBtoHSL(double r, double g, double b);
void HSVtoRGB( double fHue, double hSaturation, double hLightness );

double r, g, b;

/*! main - Mainline for APDS9960_colorS1 */

/*!
 *
 */
int main(void)
{
  unsigned int uiMailboxData[16];
  unsigned int uiOldMailbox[16];
  char szWork[32];
  int i;
  long lCount;
  unsigned int uiRed,uiGreen,uiBlue;
  double hue;

  /* Set all pins to zero, used pins will be set later */
  TRISA = 0;
  TRISB = 0;
  TRISC = 0;
  TRISD = 0;
  LATA = 0;
  LATB = 0;
  LATC = 0;
  LATD = 0;

  /* Initialize timing, serial port, etc. */
  initOscillator();
  _TRISB5 = 0;
  initializeSerialPort();   // Still not confident on baud rate generator
//  U1BRG = 159;
  U1BRG = 165;
  _LATB5 = 1;

  /* Starting banner showing compile date/time for checking */
  snore(SNORE_COUNT);
  TFTinit(TFTLANDSCAPE);
  snore(SNORE_COUNT);
  TFTsetBackColorX(BLACK);
  TFTclear();
  snore(SNORE_COUNT);
  TFTsetColorX(TURQUOISE);
  _LATB5 = 0;
  snore(SNORE_COUNT);
  TFTsetFont(FONTDJS);
  TFTputsTT("***************************************\r\n");
  TFTputsTT("***          APDS9960_color         ***\r\n");
  TFTputsTT("***       ");
  TFTputsTT(__DATE__);
  TFTputsTT(" ");
  TFTputsTT(__TIME__);
  TFTputsTT("      ***\r\n");
  TFTputsTT("***************************************\r\n");

  snore(4 * LONG_SNORE);
  TFTclear();
  /* Initialize to invalid to force initial display */
  for (i = 0; i < 16; i++)
    uiMailboxData[i] = 0xffff;
  lCount = 0;


  while (1)
    {
      // Get the data from master
      while (!SI1MBXSbits.DTRDYD) // Wait for data ready
        ;
      // Save off previous values
      for (i = 0; i < 16; i++)
        uiOldMailbox[i] = uiMailboxData[i];
      uiMailboxData[0] = SI1MBX0D;
      uiMailboxData[1] = SI1MBX1D;
      uiMailboxData[2] = SI1MBX2D;
      uiMailboxData[3] = SI1MBX3D;
      uiMailboxData[4] = SI1MBX4D;
      uiMailboxData[5] = SI1MBX5D;
      uiMailboxData[6] = SI1MBX6D;
      uiMailboxData[7] = SI1MBX7D;
      uiMailboxData[8] = SI1MBX8D;
      uiMailboxData[9] = SI1MBX9D;
      uiMailboxData[10] = SI1MBX10D;
      uiMailboxData[11] = SI1MBX11D;
      uiMailboxData[12] = SI1MBX12D;
      uiMailboxData[13] = SI1MBX13D;
      uiMailboxData[14] = SI1MBX14D;
      uiMailboxData[15] = SI1MBX15D;
      LED_ORANGE ^= 1;
#ifdef SLOW
      /* Display all mailbox data in two columns */
      for (i = 0; i < 8; i++)
        {
          if (uiMailboxData[i] != uiOldMailbox[i])
            {
              sprintf(szWork, "\033[%d;1H %2d %04x \r\n", i + 1, i, uiMailboxData[i]);
              TFTputsTT(szWork);
              snore(LINE_DELAY);
            }
        }
      for (i = 8; i < 16; i++)
        {
          if (uiMailboxData[i] != uiOldMailbox[i])
            {
              sprintf(szWork, "\033[%d;10H %2d %04x \r\n", i - 7, i, uiMailboxData[i]);
              TFTputsTT(szWork);
              snore(LINE_DELAY);
            }
        }
#endif
      /* Get red, green, blue values, might be as high as 1025 so cut */
      uiRed   = (uiMailboxData[4] << 8 | uiMailboxData[3])-2;
      uiGreen = (uiMailboxData[6] << 8 | uiMailboxData[5])-2;
      uiBlue  = (uiMailboxData[8] << 8 | uiMailboxData[7])-2;

      /* If still too large, scale down */
      if ( (uiRed>512) || (uiGreen>512) || (uiBlue>512) )
        {
          uiRed   = uiRed   >> 2;
          uiGreen = uiGreen >> 2;
          uiBlue  = uiBlue  >> 2;
        }
      if ( (uiRed>255) || (uiGreen>255) || (uiBlue>255) )
        {
          uiRed   = uiRed   >> 1;
          uiGreen = uiGreen >> 1;
          uiBlue  = uiBlue  >> 1;
        }

      /* Display the individual red, greem and blue values */
      TFTputsTT("\033[9;1H\r\n");
      sprintf(szWork, "  Red %5d\r\n", uiRed );
      TFTputsTT(szWork);
      snore(LINE_DELAY);
      sprintf(szWork, "Green %5d\r\n", uiGreen );
      TFTputsTT(szWork);
      sprintf(szWork, " Blue %5d\r\n", uiBlue );
      TFTputsTT(szWork);
      snore(LINE_DELAY);
      sprintf(szWork, "\033[14;10Hloop count %5ld ", lCount);
      TFTsetColorX(AQUAMARINE);
      TFTputsTT(szWork);
      snore(LINE_DELAY);

/*
      uiRed   = uiRed   >> 2; // Max value 1025
      uiGreen = uiGreen >> 2;
      uiBlue  = uiBlue  >> 2;
*/
      /* Display a rectangle of the measured color */
      TFTsetColor((unsigned char)uiRed,(unsigned char)uiGreen,(unsigned char)uiBlue);
      TFTputsTT("\r\n\033[5;25HThis Color\r\n");
      TFTfillRect(180,90,300,120);
      snore(LINE_DELAY);

      /* Get hue, rectangles 25, 50 and 100% lightness of same hue */
      hue = RGBtoHSL((double)uiRed,(double)uiGreen,(double)uiBlue);

      HSVtoRGB( hue, 0.99, 0.25 );
      uiRed   = (unsigned int)255.0*r;
      uiGreen = (unsigned int)255.0*g;
      uiBlue  = (unsigned int)255.0*b;
      TFTsetColor((unsigned char)uiRed,(unsigned char)uiGreen,(unsigned char)uiBlue);
      TFTfillRect(180,120,220,150);
      snore(LINE_DELAY);

      HSVtoRGB( hue, 0.99, 0.75 );
      uiRed   = (unsigned int)255.0*r;
      uiGreen = (unsigned int)255.0*g;
      uiBlue  = (unsigned int)255.0*b;
      TFTsetColor((unsigned char)uiRed,(unsigned char)uiGreen,(unsigned char)uiBlue);
      TFTfillRect(260,120,300,150);
      snore(LINE_DELAY);

      HSVtoRGB( hue, 0.99, 0.50 );
      uiRed   = (unsigned int)255.0*r;
      uiGreen = (unsigned int)255.0*g;
      uiBlue  = (unsigned int)255.0*b;
      TFTsetColor((unsigned char)uiRed,(unsigned char)uiGreen,(unsigned char)uiBlue);
      TFTfillRect(220,120,260,150);
      snore(LINE_DELAY);

      TFTputsTT("\033[11;15HLightness 25%  50%  75% ");
      snore(LINE_DELAY);

      /* Display the hue and the scaled red, green and blue values */
      sprintf(szWork,"\033[12;20Hhue=%7.5f ",hue);
      TFTsetColorX(PINK);
      TFTputsTT(szWork);
      snore(LINE_DELAY);

      sprintf(szWork,"\033[13;16HR=%5.3f G=%5.3f B=%5.3f",r,g,b);
      TFTsetColorX(SPRINGGREEN);
      TFTputsTT(szWork);
      snore(LINE_DELAY);

      TFTsetColorX(TURQUOISE);

      lCount++;

#ifdef SLOW
      snore(2 * LONG_SNORE);
#else
      snore(2*LONG_SNORE);
#endif
    }

  return 0;
}
