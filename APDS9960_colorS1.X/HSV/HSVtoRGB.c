

extern double r, g, b;

double hue2rgb(double p, double q, double t)
{
    static double rv;
    
    if(t < 0) t += 1.0;
    if(t > 1.0) t -= 1.0;
    if(t < 1.0/6.0)
	{
	    rv = (p + (q - p) * 6.0 * t);
	    return rv;
	}
    if(t < 1.0/2.0)
	{
	    rv = q;
	    return rv;
	}
    if(t < 2.0/3.0)
	{
	    rv = (p + (q - p) * (2.0/3.0 - t) * 6);
	    return rv;
	}
    rv = p;
    return rv;
}

void HSVtoRGB( double fHue, double hSaturation, double hLightness )
{
    double p,q;
    static double rl,gl,bl;

    if ( hLightness<0.5 )
	q = hLightness * (1.0+hSaturation);
    else
	q = hLightness + hSaturation - hLightness * hSaturation;
    p = 2.0 * hLightness - q;
    rl = hue2rgb(p, q, fHue + 1.0/3.0);
    gl = hue2rgb(p, q, fHue);
    bl = hue2rgb(p, q, fHue - 1.0/3.0);
    r = rl; g = gl; b = bl;
}

