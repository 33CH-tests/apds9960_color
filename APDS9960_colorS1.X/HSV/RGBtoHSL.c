#include <stdio.h>

double getmax( double n1, double n2, double n3 )
{
    double maxval;
    maxval = n1;
    if ( n2>maxval )
	maxval = n2;
    if ( n3>maxval )
	maxval = n3;
    return maxval;
}

double getmin( double n1, double n2, double n3 )
{
    double minval;
    minval = n1;
    if ( n2<minval )
	minval = n2;
    if ( n3<minval )
	minval = n3;
    return minval;
}

/**
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and l in the set [0, 1].
 *
 * @param   {number}  r       The red color value
 * @param   {number}  g       The green color value
 * @param   {number}  b       The blue color value
 * @return  {Array}           The HSL representation
 */
double RGBtoHSL(double r, double g, double b)
{
    double fMax, fMin;
    double h,s,l;
    double d;
    int isw;
    
    r /= 255.0; g /= 255.0; b /= 255.0;
    fMax = getmax(r,g,b);
    fMin = getmin(r,g,b);
    h=s=l=(fMax + fMin) / 2.0;

    if(fMax == fMin)
	{
	    h = s = 0.0; // achromatic
	}
    else
	{
	    d = fMax - fMin;
	    s = l > 0.5 ? d / (2 - fMax - fMin) : d / (fMax + fMin);
	    isw = 0;
	    if ( fMax==r )
		isw=1;
	    if ( fMax==g )
		isw=2;
	    if ( fMax==b )
		isw=3;
	    switch(isw)
		{
		case 1: h = (g - b) / d + (g < b ? 6 : 0); break;
		case 2: h = (b - r) / d + 2; break;
		case 3: h = (r - g) / d + 4; break;
		default:
		    h = 0;
		}
	    h /= 6.0;
	}

    return h;
}

