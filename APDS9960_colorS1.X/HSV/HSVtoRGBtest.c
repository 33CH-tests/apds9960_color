#include <stdio.h>

double r, g, b;

double hue2rgb(double p, double q, double t)
{
    static double rv;
    
    if(t < 0) t += 1.0;
    if(t > 1.0) t -= 1.0;
    if(t < 1.0/6.0)
	{
	    rv = (p + (q - p) * 6.0 * t);
	    //printf("%7.4f (1)\n",rv);
	    return rv;
	}
    if(t < 1.0/2.0)
	{
	    rv = q;
	    //printf("%7.4f (2)\n",rv);
	    return rv;
	}
    if(t < 2.0/3.0)
	{
	    rv = (p + (q - p) * (2.0/3.0 - t) * 6);
	    //printf("%7.4f (3)\n",rv);
	    return rv;
	}
    rv = p;
    //printf("%7.4f\n",rv);
    return rv;
}

double HSVtoRGB( double fHue, double hSaturation, double hLightness )
{
    double p,q;
    static double rl,gl,bl;

    if ( hLightness<0.5 )
	q = hLightness * (1.0+hSaturation);
    else
	q = hLightness + hSaturation - hLightness * hSaturation;
    p = 2.0 * hLightness - q;
    rl = hue2rgb(p, q, fHue + 1.0/3.0);
    gl = hue2rgb(p, q, fHue);
    bl = hue2rgb(p, q, fHue - 1.0/3.0);
    //printf("-l %6.3f %6.3f %6.3f\n",rl,gl,bl);
    r = rl; g = gl; b = bl;
    //printf("-- %6.3f %6.3f %6.3f\n",r,g,b);
    }


void main( void )
{
    double fHue;
    double fLightness;
    double fSaturation;
    unsigned int nR,nG,nB;
    unsigned long ulColor;
    int i;
    
    printf("-------\n\n");

    fLightness = 0.25;
    fSaturation = 0.99;
    i = 0;
    for ( fHue=0.0; fHue<1.016; fHue+=0.017 )
	{
	    i++;
	    HSVtoRGB(fHue,fSaturation,fLightness);
	    nR = (int)(255.0*r);
	    nG = (int)(255.0*g);
	    nB = (int)(255.0*b);
	    ulColor = ((unsigned long)nG) << 24 |
		((unsigned long)nR) << 16 |
		((unsigned long)nB) << 8;
	    printf("%3d %6.3f %6.3f %6.3f %6.3f %3d %3d %3d %08x\n",
		   i,fHue,r,g,b,nR,nG,nB,ulColor);
	}
}
