/*! \file  APDS9960_colorS1.h
 *
 *  \brief Constants and function prototypes for APDS9960_colorS1
 *
 *  \author jjmcd
 *  \date November 29, 2018, 9:23 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef APDS9960_COLORS1_H
#define	APDS9960_COLORS1_H

#ifdef	__cplusplus
extern "C"
{
#endif

#define LED_ORANGE _LATB7
//#define SNORE_COUNT   500000
//#define LONG_SNORE  50000000
#define SNORE_COUNT   50000
#define LONG_SNORE  5000000

void initOscillator(void);
void snore( long );
void initializeSerialPort();

#ifdef	__cplusplus
}
#endif

#endif	/* APDS9960_COLORS1_H */

