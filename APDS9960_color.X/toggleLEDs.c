/*! \file  toggleLEDs.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date August 2, 2018, 3:06 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "APDS9960_color.h"

/*! toggleLEDs - */

/*!
 *
 */
void toggleLEDs( int n )
{
  int i;

  for ( i=0; i<n; i++ )
    {
      _LATC9 = 1;
      snore(SNORE_COUNT);
      _LATC9 = 0;
      _LATC8 = 1;
      snore(SNORE_COUNT);
      _LATC8 = 0;
    }

}
