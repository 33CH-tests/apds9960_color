/*! \file  APDS9960_color.c
 *
 *  \brief This file contains the mainline for APDS9960_color
 *
 * Read color from APDS-9960
 *
 *
 *  \author jjmcd
 *  \date November 29, 2018, 9:20 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <libpic30.h>
#include "APDS9960_color.h"
#include "APDS9960_colorS1.h"
#include "../include/I2C.h"
#include "../include/APDS9960.h"

/*! main - Mainline for APDS9960_color */

/*!
 *
 */
int main(void)
{

  /* Disable Watch Dog Timer */
  RCONbits.SWDTEN = 0;

  /* Set all pins to zero, used pins will be set later */
  TRISA=0;
  TRISB=0;
  TRISC=0;
  TRISD=0;
  LATA=0;
  LATB=0;
  LATC=0;
  LATD=0;

  _TRISC8 = 0;  /* Blue LED */
  _TRISC9 = 0;  /* Pink LED */

  TRISA=0;
  TRISB=0;
  TRISC=0;
  TRISD=0;

  _TRISC7 = 1;  // ALS Interrupt
  ANSELCbits.ANSELC7 = 0; /* Disable analog on RC7 */

  _LATB6 = 1;
  snore(LONG_SNORE);
  toggleLEDs(3);
  _LATB6 = 0;

  /* Program and start the slave */
  _program_slave(1, 0, APDS9960_colorS1);
  _start_slave();

  /* Initialize the oscillator */
  initOscillator();
  snore(SNORE_COUNT);

  MSI1FIFOCSbits.RFEN = 1;

  /* I2C Initialization */
  I2Cinit();

  _LATC9 ^= 1;          // Toggle pink LED
  put9960(0x80,0x00);  // Start with power off
  snore(SNORE_COUNT);
  _LATC9 ^= 1;          // Toggle pink LED
  put9960(0x80,0x03);  // Power on
  snore(SNORE_COUNT);
  _LATC9 ^= 1;          // Toggle pink LED
  put9960(0x80,0x07);  // ALS enable
  snore(SNORE_COUNT);
  _LATC9 ^= 1;          // Toggle pink LED
  put9960(0x80,0x17);  // ALS interrupt enable
  snore(SNORE_COUNT);

  _LATC9 ^= 1;          // Toggle pink LED
  put9960(0x84,0x10);  // ALS low threshold low
  put9960(0x85,0x00);  // ALS low threshold high
  snore(SNORE_COUNT);

  _LATC9 ^= 1;          // Toggle pink LED
  put9960(0x86,0x10);  // ALS high threshold low
  put9960(0x87,0xf0);  // ALS high threshold high
  snore(SNORE_COUNT);

  _LATC9 ^= 1;          // Toggle pink LED
  put9960(0x8f,0x03);  // ALS gain
  snore(SNORE_COUNT);

  _LATC9 ^= 1;          // Toggle pink LED
  put9960(0x82,0x17);  // ALS integration time
  snore(SNORE_COUNT);

  while (1)
    {
      while ( _RB7 )    // Wait for data valid interrupt
        ;

      MSI1MBX0D = get9960(0x80);    // Power status

      MSI1MBX1D  = get9960(0x94);   // Clear low
      MSI1MBX2D  = get9960(0x95);   // Clear high

      MSI1MBX3D  = get9960(0x96);   // Red low
      MSI1MBX4D  = get9960(0x97);   // Red high

      MSI1MBX5D  = get9960(0x98);   // Green low
      MSI1MBX6D  = get9960(0x99);   // Green high

      MSI1MBX7D  = get9960(0x9a);   // Blue low
      MSI1MBX8D  = get9960(0x9b);   // Blue high

      MSI1MBX9D  = get9960(0x84);   // ALS low threshold low
      MSI1MBX10D = get9960(0x85);   // ALS low threshold high

      MSI1MBX11D = get9960(0x86);   // ++++++++++++++++++
      MSI1MBX12D = get9960(0x87);   // ALS high threshold high

      MSI1MBX13D = get9960(0x82);   // ALS integration time
      MSI1MBX14D = get9960(0x8f);   // ALS gain

      MSI1MBX15D = 0x5a5a;   // Automatically sets MSI1MBXSbits.DTRDYD
      put9960(0xe7,0x00);    // Clear interrupt

      _LATC9 ^= 1; // Toggle pink LED
      // Wait for slave to read data
      while( MSI1MBXSbits.DTRDYD )
        ;
    }

  return 0;
}
