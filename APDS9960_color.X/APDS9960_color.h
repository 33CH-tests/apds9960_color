/*! \file  APDS9960_color.h
 *
 *  \brief Constants and function prototypes for APDS9960_color
 *
 *  \author jjmcd
 *  \date November 29, 2018, 9:22 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef APDS9960_COLOR_H
#define	APDS9960_COLOR_H

#ifdef	__cplusplus
extern "C"
{
#endif

#define SNORE_COUNT 1000000
#define LONG_SNORE 10000000

  void initOscillator( void );
  void snore( long );
  void toggleLEDs( int );

#ifdef	__cplusplus
}
#endif

#endif	/* APDS9960_COLOR_H */

