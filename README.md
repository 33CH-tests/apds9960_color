# APDS9960_color

__Detect colors using APDS-9960 sensor__

Sensor is read periodically
by the master core. Key values sent to the slave core via mailboxes.
Slave core then uses the serial graphics terminal to create the
display.

---

![Application output](images/APDS9960_color.jpg)

---

__Displayed Fields__


*  0 - bit 0: Power on
*  0 - bit 1: ALS power on
*  0 - bit 2: ALS Enable
*  0 - bit 4: ALS Interrupt Enable
*  1 - Clear level low byte
*  2 - Clear level high byte
*  3 - Red low byte
*  4 - Red high byte
*  5 - Green low byte
*  6 - Green high byte
*  7 - Blue low byte
*  8 - Blue high byte
*  9 - ALS low threshold low byte
* 10 - ALS low threshold high byte
* 11 - ALS high threshold low byte
* 12 - ALS high threshold high byte
* 13 - ALS Integration time
* 14 - ALS gain
* 15 - 0x5a5a (terminator)

----

The red, green and blue compnents varied significantly. Whether there was
simply too much noise on the signal, or whether it was just too tricky to
hold the filters stii, I can't be sure.

I then calculated the hue, and drew rectangles of the original color, along
with rectangles of the correct hue with 100% saturation and 25%, 50% and
75% lightness. (The TFT tends to photograph with a bit of a blue tint.)

----

![Application output](images/FourColors.jpg)

----
